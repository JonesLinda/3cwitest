package at.test;

/**
 * This class encapsulates - Songs
 *
 * @author Jones
 * <pre>
 *          ID   Date        Description
 *          VJ   01.06.2021  New creation
 *          </pre>
 */
public class Song {
    private String name;
    private int lenght;

    public Song(String name, int lenght){
        this.name = name;
        this.lenght = lenght;
    }

    public int getLenght(){

        return lenght;
    }
}
