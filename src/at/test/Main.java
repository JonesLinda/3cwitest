package at.test;

/**
 * This class encapsulates - Main
 *
 * @author Jones
 * <pre>
 *          ID   Date        Description
 *          VJ   01.06.2021  New creation
 *          </pre>
 */
public class Main {
    public static void main(String[] args) {
        Vinyl vinyl = new Vinyl("Steven Universe", 1);
        Vinyl vinyl1 = new Vinyl("Carly", 2);
        Vinyl vinyl2 = new Vinyl("Lofi", 3);

        MusicBox musicBox = new MusicBox();

        musicBox.addRecord(vinyl);
        musicBox.addRecord(vinyl2);

        musicBox.removeRecord(vinyl1);

       musicBox.getFreespace();

       musicBox.addRecord(vinyl);

       musicBox.loadDisk(vinyl1);

       musicBox.playRecord(vinyl2);


    }
}
