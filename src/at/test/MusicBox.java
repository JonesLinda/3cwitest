package at.test;

import java.util.ArrayList;
import java.util.List;

/**
 * This class encapsulates - MusicBox
 *
 * @author Jones
 * <pre>
 *          ID   Date        Description
 *          VJ   01.06.2021  New creation
 *          </pre>
 */
public class MusicBox {
    private boolean capacity = true;
    private Vinyl vinyl;
    private List<Vinyl> vinyls = new ArrayList<>();

    public MusicBox(){
        this.vinyls = new ArrayList<>();
    }
    public void getFreespace(){
        int sum = 0;
        for (Vinyl vinyl:vinyls) {
            sum++;
        }
        if (sum > 50){
            vinyls.remove(50);
            System.out.println("insuficient Space Left ");
            capacity = false;
        }

    }
    public void addRecord(Vinyl vinyl){
        vinyls.add(vinyl);
        getFreespace();

        if (capacity)
            System.out.println(vinyl.getRecord() + " has been added");

    }
    public void removeRecord(Vinyl vinyl){
        vinyls.remove(vinyl);
        System.out.println(vinyl.recordTitle() + " has been removed");

    }

    public void loadDisk(Vinyl vinyl) {
        System.out.println(vinyl.recordTitle() + " is loaded");
    }
    public void playRecord(Vinyl vinyl){
        System.out.println("curently playing: " + vinyl.recordTitle());
    }

}
