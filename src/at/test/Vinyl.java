package at.test;

import java.util.ArrayList;
import java.util.List;

/**
 * This class encapsulates - Vinyl
 *
 * @author Jones
 * <pre>
 *          ID   Date        Description
 *          VJ   01.06.2021  New creation
 *          </pre>
 */
public class Vinyl {
    private String title;
    private int ID;

    private List<Song>songs;

    public Vinyl(String title, int ID){
        this.title = title;
        this.ID = ID;

        this.songs = new ArrayList<>();
    }

    public int getSumOfMusic(){
        int sum = 0;
        for (Song song: songs) {
            sum += song.getLenght();
        }
        return sum;


    }

    public void loadRecord(String Record){
        System.out.println("loading: " + Record);
    }

    public String getRecord(){
        return this.ID + " " + this.title;
    }

    public String recordTitle(){
        return this.title;


        }
    }

